# React + Tailwindcss

## Getting started

Record your affirmations into reality and create animated video with subtitles with sound mixing.

## 🚀 Technology Used
- Design
  - Figma to react + tailwind
  - Mobile Responsive
- Libraries
  - Wavesurfer
- PHP v8.1
- Laravel v10
- Inertia JS
- React JS
- MySql
- PSR 4 Standard ( As client required )

# Installation
- Clone the repository: git clone https://gitlab.com/git_design_lr/react-tailwindcss
- Open the project in VS Code.
- Go to project folder directory
- Run 'npm install'
- Run 'npm start'

# Usage

- Create four steps
  - Step 1: Record my voice with transcription
  - Step 2: Sound mixing (Merge the two diffrent 
            audio sound with recorded sound)
  - Step 3: Upload multiple images and select 
            the effect type (Kaleidoscop / Zoom in zoom out)
  - Step 4: Play Audio / Review Audio

## 🖼️ Screenshots

![App Screenshot](https://i.imgur.com/eRfeZsi.png)

![App Screenshot](https://i.imgur.com/GhmVZyj.png)

