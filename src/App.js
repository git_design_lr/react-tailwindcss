import "./App.css";
import Blog from "./components/Blog/Blog";
import CreateAfirmation from "./components/Create-Afirmation/CreateAfirmation";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Home from "./components/Home/Home";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MyAfirmation from "./components/My-Afirmation/MyAfirmation";

function App() {
  return (
    <div className=" dark:bg-black min-h-screen bg-dark">
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/blog" element={<Blog />} />
          <Route path='/affirmation.create' element={<CreateAfirmation />} />
          <Route path='/affirmation' element={<MyAfirmation />} />
        </Routes>
        <Footer />
      </BrowserRouter>
    </div>
  );
}

export default App;
