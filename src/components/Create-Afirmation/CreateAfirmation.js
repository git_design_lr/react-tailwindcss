import React from 'react'

const CreateAfirmation = () => {
  return (
    <main>
      <div className="bg-blue text-white mt-10 mb-5 w-max block m-auto px-6 py-1 rounded">
        Record Your Affirmations
      </div>
      <div className=" p-4 xs:mb-2 mb-14 max-w-2xl mx-auto w-11/12">
        <div className="flex items-center">
          <div className="flex items-center relative">
            <div className="rounded-full flex items-center justify-center transition duration-500 ease-in-out h-8 w-8 py-3 border-2 border-primary cursor-pointer">
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth="0"
                viewBox="0 0 16 16"
                className="text-lg text-blue"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <circle cx="8" cy="8" r="8"></circle>
              </svg>
            </div>
            <div className="absolute top-0 -ml-12 text-center mt-14 w-32 text-xs font-medium capitalize text-white xs:hidden">
              Record Your Affirmations
            </div>
          </div>
          <div className="flex-auto border-t-2 transition duration-500 ease-in-out border-gray-500 mx-5"></div>
          <div className="flex items-center relative">
            <div className="rounded-full flex items-center justify-center transition duration-500 ease-in-out h-8 w-8 py-3 border-2 border-gray-500"></div>
            <div className="absolute top-0 -ml-12 text-center mt-14 w-32 text-xs font-medium capitalize text-white xs:hidden">
              Sound Mixing
            </div>
          </div>
          <div className="flex-auto border-t-2 transition duration-500 ease-in-out border-gray-500 mx-5"></div>
          <div className="flex items-center relative">
            <div className="rounded-full flex items-center justify-center transition duration-500 ease-in-out h-8 w-8 py-3 border-2 border-gray-500"></div>
            <div className="absolute top-0 -ml-12 text-center mt-14 w-32 text-xs font-medium capitalize text-white xs:hidden">
              Upload Images
            </div>
          </div>
          <div className="flex-auto border-t-2 transition duration-500 ease-in-out border-gray-500 mx-5"></div>
          <div className="flex items-center relative">
            <div className="rounded-full flex items-center justify-center transition duration-500 ease-in-out h-8 w-8 py-3 border-2 border-gray-500"></div>
            <div className="absolute top-0 -ml-12 text-center mt-14 w-32 text-xs font-medium capitalize text-white xs:hidden">
              Play Video
            </div>
          </div>
        </div>
      </div>
      <div className="max-w-7xl mx-auto xs:pt-0 py-6 px-4 sm:px-6 lg:px-8">
        <div className="rounded bg-box p-6 sm:p-10 xs:mb-2 mb-5">
          <div className="text-white text-md font-semibold mb-5">Affirmation Ideas</div>
          <div className="grid grid-cols-1 -xs:grid-cols-2 sm:grid-cols-2 gap-14">
            <div>
              <div className="">
                <label
                  htmlFor="email"
                  className="block mb-3 font-normal text-sm  text-primary"
                >
                  Select a affirmation category
                </label>
                <select
                  className="p-3 bg-input outline-none border-none  focus:border-none focus:ring-0 focus:shadow-none focus:outline-0  rounded-md shadow-sm text-white minimal w-full cursor-pointer"
                  name="affirmation_category"
                >
                  <option value="">Select category</option>
                  <option value="8">Visual Arts Affirmations</option>
                  <option value="5">Performing Arts Affirmation</option>
                  <option value="3">Wealth Affirmation</option>
                  <option value="2">Health Affirmation</option>
                  <option value="1">General Affirmation</option>
                </select>
              </div>
              <div className="mt-4">
                <label
                  htmlFor="text"
                  className="block mb-3 font-normal text-sm  text-primary"
                >
                  Affirmation Name
                </label>
                <input
                  id="text"
                  name="text"
                  placeholder="Add affirmation name"
                  autoComplete="off"
                  type="text"
                  className="p-3 bg-input outline-none border-none  focus:border-none focus:ring-0 focus:shadow-none focus:outline-0  rounded-md shadow-sm text-whitemt-1 block w-full text-white"

                />
              </div>
            </div>
            <div>
              <h4 className="text-primary text-sm font-medium">
                Visual Arts Affirmations
              </h4>
              <div className="show-category-list list-disc pl-5 mt-3 text-white text-md font-light">
                <ul className="list-disc">
                  <li>I attract abundance and prosperity into my life.</li>
                  <li>I am worthy of financial success and wealth.</li>
                  <li>I am grateful for the abundance that surrounds me.</li>
                  <li>Visual Affirmations demo test</li>
                  <li>I am open to receiving all the wealth and success that comes my way.</li>
                </ul>
              </div>
            </div>
          </div>{" "}
          <div className="text-xs text-gray-400 text-center max-w-sm w-full mx-auto mt-14">
            Click the “Allow” button to enable microphone access for recording
            your affirmations.
          </div>
          <div className="flex items-center justify-center mt-5 gap-3 sm:gap-5 xs:flex-wrap">
            <button className="inline-flex items-center justify-center px-2 sm:px-4 py-3 bg-blue border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-blue-700 focus:bg-blue  active:bg-blue focus:outline-none  focus:ring-0 focus:ring-offset-0 transition ease-in-out duration-150 text-center undefined capitalize xs:w-full">
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth="0"
                viewBox="0 0 16 16"
                className="text-lg mr-2 justify-center"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M5 3a3 3 0 0 1 6 0v5a3 3 0 0 1-6 0V3z"></path>
                <path d="M3.5 6.5A.5.5 0 0 1 4 7v1a4 4 0 0 0 8 0V7a.5.5 0 0 1 1 0v1a5 5 0 0 1-4.5 4.975V15h3a.5.5 0 0 1 0 1h-7a.5.5 0 0 1 0-1h3v-2.025A5 5 0 0 1 3 8V7a.5.5 0 0 1 .5-.5z"></path>
              </svg>{" "}
              Start Recording
            </button>
          </div>
          <div className="mt-6 text-center">
            <label
              htmlFor="text"
              className="block mb-3 font-normal text-sm  text-white uppercase font-semibold"
            >
              Transcription Box
            </label>
            <textarea className="p-3 rounded max-w-xl w-full h-36 border-0 bg-input text-white focus:outline-none focus:border-0 focus:shadow-0 focus:ring-0"></textarea>
          </div>
          <div className="flex justify-center mt-5 gap-5">
            <button className="inline-flex items-center px-5 py-3 border-0 rounded text-sm font-semibold capitalize xs:w-full xs:justify-center bg-pink-700 text-white">
              Save{" "}
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth="0"
                viewBox="0 0 16 16"
                className="text-lg ml-2"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v7.293l2.646-2.647a.5.5 0 0 1 .708.708l-3.5 3.5a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L7.5 9.293V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z"></path>
              </svg>
            </button>
            <button
              type="button"
              className="inline-flex items-center px-5 py-3 bg-primary border-0 rounded font-semibold text-black uppercase tracking-widest shadow-sm text-sm   focus:outline-none  disabled:opacity-25 transition ease-in-out duration-150 undefined capitalize xs:w-full xs:justify-center ml-3"
            >
              Next{" "}
              <svg
                stroke="currentColor"
                fill="currentColor"
                strokeWidth="0"
                viewBox="0 0 16 16"
                className="text-lg ml-1"
                height="1em"
                width="1em"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
                ></path>
              </svg>
            </button>
          </div>
        </div>
      </div>
    </main>
  )
}

export default CreateAfirmation