import { useState } from "react";
import React from 'react';
import { Link } from "react-router-dom";
import Logo from '../../assets/images/logo.svg';

function Header() {
    const [navbar, setNavbar] = useState(false);

    return (

        <nav className="w-full bg-primary shadow">
            <div className="max-w-7xl mx-auto  justify-between py-3 mx-auto md:items-center md:flex px-2">
                <div>
                    <div className="flex items-center justify-between md:block">
                        <Link to="/">
                            <img src={Logo} alt="img-logo" className="w-[180px] ml-3 md:w-[210]" />
                        </Link>
                        <div className="md:hidden">
                            <button
                                className="p-2 text-gray-700 rounded-md outline-none border-0"
                                onClick={() => setNavbar(!navbar)}
                            >
                                {navbar ? (
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="w-6 h-6 "
                                        viewBox="0 0 20 20"
                                        fill="currentColor"
                                    >
                                        <path
                                            fillRule="evenodd"
                                            d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                            clipRule="evenodd"
                                        />
                                    </svg>
                                ) : (
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        className="w-6 h-6"
                                        fill="none"
                                        viewBox="0 0 24 24"
                                        stroke="currentColor"
                                        strokeWidth={2}
                                    >
                                        <path
                                            strokeLinecap="round"
                                            strokeLinejoin="round"
                                            d="M4 6h16M4 12h16M4 18h16"
                                        />
                                    </svg>
                                )}
                            </button>
                        </div>
                    </div>
                </div>
                <div>
                    <div
                        className={`flex-1 justify-self-center pb-3 mt-8 md:block md:pb-0 md:mt-0 ${navbar ? "block" : "hidden"
                            }`}
                    >
                        <ul className="items-center justify-center space-y-4 ml-5 md:ml-0 md:flex md:space-x-6 md:space-y-0">

                            <Link
                                to={"/"} className="text-gray-900 hover:text-indigo-600 block">
                               Home
                            </Link>
                            <Link
                                to={"/affirmation.create"} className="text-gray-900 hover:text-indigo-600 block">
                               Record Your Affirmations
                            </Link>
                            <Link
                                to={"/affirmation"} className="text-gray-900 hover:text-indigo-600 block">
                                My Affirmations
                            </Link>
                        </ul>
                    </div>
                </div>

            </div>
        </nav>

    );
}

export default Header;
