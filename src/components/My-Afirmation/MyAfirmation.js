import React from "react";

const MyAfirmation = () => {
  return (
    <main>
      <div className="bg-blue text-white mt-5 mb-5 w-max block m-auto px-6 py-1 rounded">
        My Affirmations
      </div>
      <div className="min-h-[calc(75vh-14px)]">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
          <div className="bg-box rounded my-3 p-5">
            <div className="sm:flex items-center justify-between">
              <div className="border-1 border-b border-gray-600 sm:border-none pb-2 sm:pb-0">
                <h6 className="text-white text-sm font-medium">test</h6>
                <p className="text-gray-300 text-xs mt-1">
                  Last Modification : 17-Jul-2023 | 05:09
                </p>
              </div>
              <div className="flex items-center xs:justify-start justify-between sm:justify-start pt-2 sm:pt-0">
                <div className="flex items-center gap-4">
                  <div className="music-wave-Warpper text-white">
                    <div className="flex items-center justify-center text-2xl text-white">
                      <div className="music-outer flex items-center">
                        <button className="w-[50px]">
                          <svg
                            stroke="currentColor"
                            fill="currentColor"
                            strokeWidth="0"
                            viewBox="0 0 16 16"
                            className="text-4xl text-white cursor-pointer me-3"
                            height="1em"
                            width="1em"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"></path>
                          </svg>
                        </button>
                        <div className="font-mono pt-[5px]">00:03</div>
                      
                          
                      </div>
                    </div>
                  </div>
                </div>
                <div className="text-blue text-5xl font-light overflow-hidden  mx-3">
                  |
                </div>
                <div className="flex items-center gap-3">
                  <a
                    title="Edit"
                    href="#!"
                  >
                    <div className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer">
                      <svg
                        stroke="currentColor"
                        fill="currentColor"
                        strokeWidth="0"
                        viewBox="0 0 24 24"
                        className="text-primary"
                        height="1em"
                        width="1em"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path d="M21 15.243v5.765a.993.993 0 0 1-.993.992H3.993A1 1 0 0 1 3 20.993V9h6a1 1 0 0 0 1-1V2h10.002c.551 0 .998.455.998.992v3.765l-8.999 9-.006 4.238 4.246.006L21 15.243zm.778-6.435l1.414 1.414L15.414 18l-1.416-.002.002-1.412 7.778-7.778zM3 7l5-4.997V7H3z"></path>
                        </g>
                      </svg>
                    </div>
                  </a>
                  <div
                    className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer"
                    title="Delete"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 24 24"
                      className="text-primary"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                        <path d="M17 6h5v2h-2v13a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8H2V6h5V3a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v3zm-8 5v6h2v-6H9zm4 0v6h2v-6h-2zM9 4v2h6V4H9z"></path>
                      </g>
                    </svg>
                  </div>
                  <a
                    href="#!"
                    title="Download"
                    className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer"
                  >
                    <svg
                      stroke="currentColor"
                      fill="none"
                      strokeWidth="2"
                      viewBox="0 0 24 24"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="text-primary"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                      <path d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2 -2v-2"></path>
                      <path d="M7 11l5 5l5 -5"></path>
                      <path d="M12 4l0 12"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>{" "}
          </div>
          <div className="bg-box rounded my-3 p-5">
            <div className="sm:flex items-center justify-between">
              <div className="border-1 border-b border-gray-600 sm:border-none pb-2 sm:pb-0">
                <h6 className="text-white text-sm font-medium">Second</h6>
                <p className="text-gray-300 text-xs mt-1">
                  Last Modification : 30-Jun-2023 | 11:30
                </p>
              </div>
              <div className="flex items-center xs:justify-start justify-between sm:justify-start pt-2 sm:pt-0">
                <div className="flex items-center gap-4">
                  <div className="music-wave-Warpper text-white">
                    <div className="flex items-center justify-center text-2xl text-white">
                      <div className="music-outer flex items-center">
                        <button className="w-[50px]">
                          <svg
                            stroke="currentColor"
                            fill="currentColor"
                            strokeWidth="0"
                            viewBox="0 0 16 16"
                            className="text-4xl text-white cursor-pointer me-3"
                            height="1em"
                            width="1em"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"></path>
                          </svg>
                        </button>
                        <div className="font-mono pt-[5px]">00:43</div>

                      </div>
                    </div>
                  </div>
                </div>
                <div className="text-blue text-5xl font-light overflow-hidden  mx-3">
                  |
                </div>
                <div className="flex items-center gap-3">
                  <a
                    title="Edit"
                    href="#!"
                  >
                    <div className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer">
                      <svg
                        stroke="currentColor"
                        fill="currentColor"
                        strokeWidth="0"
                        viewBox="0 0 24 24"
                        className="text-primary"
                        height="1em"
                        width="1em"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path d="M21 15.243v5.765a.993.993 0 0 1-.993.992H3.993A1 1 0 0 1 3 20.993V9h6a1 1 0 0 0 1-1V2h10.002c.551 0 .998.455.998.992v3.765l-8.999 9-.006 4.238 4.246.006L21 15.243zm.778-6.435l1.414 1.414L15.414 18l-1.416-.002.002-1.412 7.778-7.778zM3 7l5-4.997V7H3z"></path>
                        </g>
                      </svg>
                    </div>
                  </a>
                  <div
                    className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer"
                    title="Delete"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 24 24"
                      className="text-primary"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                        <path d="M17 6h5v2h-2v13a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8H2V6h5V3a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v3zm-8 5v6h2v-6H9zm4 0v6h2v-6h-2zM9 4v2h6V4H9z"></path>
                      </g>
                    </svg>
                  </div>
                  <a
                    href="#!"
                    title="Download"
                    className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer"
                  >
                    <svg
                      stroke="currentColor"
                      fill="none"
                      strokeWidth="2"
                      viewBox="0 0 24 24"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="text-primary"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                      <path d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2 -2v-2"></path>
                      <path d="M7 11l5 5l5 -5"></path>
                      <path d="M12 4l0 12"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>{" "}
          </div>
          <div className="bg-box rounded my-3 p-5">
            <div className="sm:flex items-center justify-between">
              <div className="border-1 border-b border-gray-600 sm:border-none pb-2 sm:pb-0">
                <h6 className="text-white text-sm font-medium">
                  Other sound mixing
                </h6>
                <p className="text-gray-300 text-xs mt-1">
                  Last Modification : 30-Jun-2023 | 07:12
                </p>
              </div>
              <div className="flex items-center xs:justify-start justify-between sm:justify-start pt-2 sm:pt-0">
                <div className="flex items-center gap-4">
                  <div className="music-wave-Warpper text-white">
                    <div className="flex items-center justify-center text-2xl text-white">
                      <div className="music-outer flex items-center">
                        <button className="w-[50px]">
                          <svg
                            stroke="currentColor"
                            fill="currentColor"
                            strokeWidth="0"
                            viewBox="0 0 16 16"
                            className="text-4xl text-white cursor-pointer me-3"
                            height="1em"
                            width="1em"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM6.79 5.093A.5.5 0 0 0 6 5.5v5a.5.5 0 0 0 .79.407l3.5-2.5a.5.5 0 0 0 0-.814l-3.5-2.5z"></path>
                          </svg>
                        </button>
                        <div className="font-mono pt-[5px]">00:38</div>

                      </div>
                    </div>
                  </div>
                </div>
                <div className="text-blue text-5xl font-light overflow-hidden  mx-3">
                  |
                </div>
                <div className="flex items-center gap-3">
                  <a
                    title="Edit"
                    href="#!"
                  >
                    <div className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer">
                      <svg
                        stroke="currentColor"
                        fill="currentColor"
                        strokeWidth="0"
                        viewBox="0 0 24 24"
                        className="text-primary"
                        height="1em"
                        width="1em"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <g>
                          <path fill="none" d="M0 0h24v24H0z"></path>
                          <path d="M21 15.243v5.765a.993.993 0 0 1-.993.992H3.993A1 1 0 0 1 3 20.993V9h6a1 1 0 0 0 1-1V2h10.002c.551 0 .998.455.998.992v3.765l-8.999 9-.006 4.238 4.246.006L21 15.243zm.778-6.435l1.414 1.414L15.414 18l-1.416-.002.002-1.412 7.778-7.778zM3 7l5-4.997V7H3z"></path>
                        </g>
                      </svg>
                    </div>
                  </a>
                  <div
                    className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer"
                    title="Delete"
                  >
                    <svg
                      stroke="currentColor"
                      fill="currentColor"
                      strokeWidth="0"
                      viewBox="0 0 24 24"
                      className="text-primary"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g>
                        <path fill="none" d="M0 0h24v24H0z"></path>
                        <path d="M17 6h5v2h-2v13a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8H2V6h5V3a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v3zm-8 5v6h2v-6H9zm4 0v6h2v-6h-2zM9 4v2h6V4H9z"></path>
                      </g>
                    </svg>
                  </div>
                  <a
                    href="#!"
                    title="Download"
                    className="bg-input rounded-full w-9 h-9 flex items-center justify-center cursor-pointer"
                  >
                    <svg
                      stroke="currentColor"
                      fill="none"
                      strokeWidth="2"
                      viewBox="0 0 24 24"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      className="text-primary"
                      height="1em"
                      width="1em"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
                      <path d="M4 17v2a2 2 0 0 0 2 2h12a2 2 0 0 0 2 -2v-2"></path>
                      <path d="M7 11l5 5l5 -5"></path>
                      <path d="M12 4l0 12"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default MyAfirmation;
