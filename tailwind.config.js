/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ['./src/**/*.{html,js,jsx}', './public/index.html'],
  theme: {
    extend: {
      // fontFamily: {
      //   sans: ['Montserrat'],
      // },
      colors: {
        primary: '#C4FD4A', // Replace with your desired color
        green: '#1C183D',
        'dark': '#110E2B',
        'box': '#1C183D',
        'blue': '#5653F6',
        'blue-700': '#726ff1',
        'red': '#E1284A',
        'input': '#494664',
      },
    },
    screens: {
      'md': '992px',
      'sm': '768px',
      'xs': { 'max': '475px' },
    },


  },
  plugins: [],
}


